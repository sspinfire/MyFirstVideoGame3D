﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;




public class PlayerController : MonoBehaviour {


    //movement variable
    public float runSpeed;
    public float walkSpeed;



    Rigidbody myRB;

    Animator myAnim;

    bool fancingRight;


    //for jumping
    bool grounded = false;

    Collider[] groundCollisions;
    float groundCheckRadius = 0.2f;
    public LayerMask groundLayer;
    public Transform groundCheck;
    public float jumpHeight;


	// Use this for initialization
	void Start () 
    {
        myRB = GetComponent<Rigidbody>();
        myAnim = GetComponent<Animator>();
        fancingRight = true;
	}
	
	// Update is called once per frame
	void Update () 
    {
		
	}

    public void FixedUpdate()
    {
        if(grounded && Input.GetAxis("Jump")>0)
        {
            grounded = false;
            myAnim.SetBool("grounded", grounded);
            myRB.AddForce(new Vector3(0, jumpHeight, 0));
        }

        groundCollisions = Physics.OverlapSphere(groundCheck.position, groundCheckRadius, groundLayer);

        if (groundCollisions.Length > 0) grounded = true;
        else grounded = false;

        myAnim.SetBool("grounded", grounded);



        float move = Input.GetAxis("Horizontal");
        myAnim.SetFloat("Speed", Mathf.Abs(move));

        float sneaking = Input.GetAxisRaw("Fire3");
        myAnim.SetFloat("sneaking", sneaking);


        if(sneaking>0 && grounded)
        {
            myRB.velocity = new Vector3(move * walkSpeed, myRB.velocity.y, 0);    
        }
        else{
            myRB.velocity = new Vector3(move * runSpeed, myRB.velocity.y, 0);
        }






        if (move > 0 && fancingRight != true) Flip();    
        else if(move <= 0 && fancingRight) Flip();

        
    }

    public void Flip()
    {
        fancingRight = !fancingRight;
        Vector3 theScale = transform.localScale;
        theScale.z *= -1;
        transform.localScale = theScale;
    }
}
 